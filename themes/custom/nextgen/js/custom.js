jQuery(window).on('load', function () {
    jQuery('.menu-toggle--show').click(function () {
        jQuery('ul.menu').slideDown('fast')
    });
    jQuery('.menu-toggle--hide').click(function () {
        jQuery('ul.menu').slideUp('fast')
    });
    jQuery('.path-frontpage').css('visibility', 'visible');
    
    jQuery(".views_slideshow_cycle_slide.views_slideshow_slide").each(function(){
        jQuery(this).find( ".views-field-field-description, .views-field-field-title" ).wrapAll( "<div class='banner_caption' />");
        jQuery(this).find(".banner_caption" ).before( "<div class='barslide'></div>" );
    });
});